FROM niklashh/sourcepawn:latest

COPY --chown=steam ./scripting ./addons/sourcemod/scripting

RUN [ "/bin/bash", \
    "-c", \
    "./addons/sourcemod/scripting/compile.sh ssj.sp \
    && mv addons/sourcemod/scripting/compiled/ssj.smx addons/sourcemod/plugins \
    && tar cvf ssj.tar.gz \
    addons/sourcemod/scripting/{ssj.sp,include/shavit.inc,include/dynamic-basic.inc,include/multicolors.inc,include/dynamic.inc,include/dynamic-collection.inc,/include/multicolors} \
    addons/sourcemod/plugins/ssj.smx" ]

